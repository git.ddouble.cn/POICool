package ddouble;

import ddouble.domain.Demo;
import ddouble.export.nongeneric.ExcelExportXLSX;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
        String[] headers1 = {
                "name",
                "gender",
                "location"
        };

        List<Object> dataset = new ArrayList<Object>();

        dataset.add(new Demo("aa","nan", 11L));

        String pathname ="/home/tu/aaa.xlsx";

        new ExcelExportXLSX().generator(dataset,pathname,headers1,"null",null);
    }
}
