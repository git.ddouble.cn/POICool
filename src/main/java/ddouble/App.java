package ddouble;

import ddouble.domain.Demo;
import ddouble.export.GenericGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class App<T>
{
    public static void main( String[] args )
    {

        String[] headers1 = {
                "name",
                "gender",
                "location"
        };

        List<Demo> dataset = new ArrayList<Demo>();

        dataset.add(new Demo("aa","nan", 11L));

        String pathname ="/home/tu/aaa.xls";

        new GenericGenerator<Demo>().generator(dataset,pathname,headers1,"hh",null);
    }
}
