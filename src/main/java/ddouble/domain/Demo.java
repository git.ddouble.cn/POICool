package ddouble.domain;

/**
 * Created by tu on 17-1-15.
 */
public class Demo {
    String name;
    String gender;
    Long location;

    public Demo(String name, String gender, Long loc){
        this.name = name;
        this.gender = gender;
        this.location = loc;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setLocation(Long location) {
        this.location = location;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLocation() {
        return location;
    }

    public String getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }
}
