package ddouble.export.generic;

import java.util.List;

/**
 * Created by tu on 17-1-15.
 */
public interface IExcelExport<T> {

    public void generator(List<T> dataset, String filepath,
                            String[] headers, String title,
                            String pattern);
}
