package ddouble.export;


import ddouble.export.nongeneric.ExcelExportXLS;
import ddouble.export.nongeneric.ExcelExportXLSX;

import java.util.List;

/**
 * Created by tu on 17-1-30.
 */
public class NonGenericGenerator {
    public void generator(List<Object> dataset, String filepath,
                          String[] headers, String title,
                          String pattern){
        //     /home/tu/aaa.xls ==> home | tu | aaa.xls
        String[] temp = filepath.split("/");
        String fileName = temp[temp.length-1];
        //     aaa.xls ==> xls
        String[] temp1 = fileName.split("\\.");
        String filetype = temp1[1];
        //
        if("xls".equals(filetype)){
            new ExcelExportXLS().generator(dataset,filepath,headers,title,pattern);

        }else if("xlsx".equals(filetype)){
            new ExcelExportXLSX().generator(dataset,filepath,headers,title,pattern);
        }else{
            System.out.println("暂时不支持这种格式");
        }
    }
}
