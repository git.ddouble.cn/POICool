package ddouble.export;

import ddouble.export.generic.ExcelExportXLS;
import ddouble.export.generic.ExcelExportXLSX;

import java.util.List;

/**
 * Created by tu on 17-1-16.
 */
public class GenericGenerator<T> {
    public void generator(List<T> dataset, String filepath,
                                 String[] headers, String title,
                                 String pattern){
        //     /home/tu/aaa.xls ==> home | tu | aaa.xls
        String[] temp = filepath.split("/");
        String fileName = temp[temp.length-1];
        //     aaa.xls ==> xls
        String[] temp1 = fileName.split("\\.");
        String filetype = temp1[1];
        //
        if("xls".equals(filetype)){
            new ExcelExportXLS<T>().generator(dataset,filepath,headers,title,pattern);

        }else if("xlsx".equals(filetype)){
            new ExcelExportXLSX<T>().generator(dataset,filepath,headers,title,pattern);
        }else{
            System.out.println("暂时不支持这种格式");
        }
    }
}
