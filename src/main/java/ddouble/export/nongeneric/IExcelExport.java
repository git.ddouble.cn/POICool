package ddouble.export.nongeneric;

import java.util.List;

/**
 * Created by tu on 17-1-30.
 */
public interface IExcelExport {
    public void generator(List<Object> dataset, String filepath,
                          String[] headers, String title,
                          String pattern);
}
