# 功能说明

##POI数据导出到Excel：

参数输入: List< Obj >
结果输出: Excel

要求：
v-0.0.1（没有打包）:支持配置列名，工作表名，文件名，文件格式
v-0.0.2（没有打包）:给予多个List<Obj>放到多个工作薄的sheet里面

### 泛型模式
```
    public void testApp()
    {
        assertTrue( true );

        String[] headers1 = {
                "name",
                "gender",
                "location"
        };

        List<Demo> dataset = new ArrayList<Demo>();

        dataset.add(new Demo("aa","nan", 11L));

        String pathname ="/home/tu/aaa.xls";
        String pathname1 ="/home/tu/aaa.xlsx";

        //这里用到了一个简单工厂模式：目的是为了生成{xls，xlsx}两种不同格式的excel        
        new GeneratorFactory<Demo>().generator(dataset,pathname1,headers1,"sheet1",null);

    }
```
#### 非泛型模式
```
public void testApp()
    {
        assertTrue( true );

        String[] headers1 = {
                "name",
                "gender",
                "location"
        };

        List<Object> dataset = new ArrayList<Object>();

        dataset.add(new Demo("aa","nan", 11L));

        String pathname ="/home/tu/aaa.xls";
        String pathname1 ="/home/tu/aaa.xlsx";

        //这里用到了一个简单工厂模式：目的是为了生成{xls，xlsx}两种不同格式的excel        
        new GeneratorFactory.generator(dataset,pathname1,headers1,"sheet1",null);

    }
```
## POI数据导出为Json

## 附图说明
![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/161027_8220da89_662339.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/161112_04563f16_662339.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/161141_4c5e42f0_662339.png "在这里输入图片标题")

### 感谢

借鉴了github上面的一位大佬的代码，链接在此https://github.com/T5750/poi  
并做了我个人的整理。

### 作者

DDouble(Ddouble)
